#!/bin/bash
echo "====================================";
echo "Hola, soy rudolf e instalare docker y docker compose en su computadora.";  
echo "Empezemos actualizando su sistema."
echo "====================================";
echo ""

sudo apt update
sudo apt upgrade

echo ""
echo "====================================";
echo "Su sistema ha sido actualizado correctamente.";
echo "====================================";

echo ""
echo "====================================";
echo "Es momento de preparar su computadora para la instalacion de docker";  
echo "Por lo quev eliminare cualquier version de docker que ya este instalda en el sistema";
echo "====================================";
echo ""

sudo apt-get remove docker docker-engine docker.io containerd runc

echo ""
echo "====================================";
echo "Actualizare el sistema para instalar los primeros paquetes.";
echo "====================================";
echo ""

sudo apt-get update

echo ""
echo "====================================";
echo "A continaucion instalare los certificados ca, curl, gnupg y lsb-release.";
echo "====================================";
echo ""

sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

echo ""
echo "====================================";
echo "Ahora agregare la clave gpg oficial de docker a su computadora.";
echo "====================================";
echo ""

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo ""
echo "====================================";
echo "Toca el turno de configurar la version estable del repositorio.";
echo "====================================";
echo ""

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

echo ""
echo "====================================";
echo "Actualizre su sitema por ultima vez.";
echo "====================================";
echo ""

sudo apt-get update

echo ""
echo "====================================";
echo "Ha llegado el momento de instalar docker.";
echo "====================================";
echo ""

sudo apt-get install docker-ce docker-ce-cli containerd.io

echo ""
echo "====================================";
echo "Verificare que docker ha sido instalado correctamente.";
echo "====================================";
echo ""

sudo docker run hello-world

echo ""
echo "====================================";
echo "Docker ha sido instalado correctamente.";
echo "====================================";
echo ""

echo ""
echo "====================================";
echo "Ahora instalare docker compose.";
echo "====================================";
echo ""

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

echo ""
echo "====================================";
echo "Verificare que docker compose ha sido instalado correctamente.";
echo "====================================";
echo ""

docker-compose --version

echo ""
echo "====================================";
echo "Docker compose ha sido instalado correctamente.";
echo "====================================";
echo ""

echo ""
echo "====================================";
echo "Procedere con la instalacion de homebrew.";
echo "====================================";
echo ""

echo ""
echo "====================================";
echo "Actualizare el sistema.";
echo "====================================";
echo ""

sudo apt update

sudo apt-get install build-essential

echo ""
echo "====================================";
echo "Instalare git.";
echo "====================================";
echo ""

sudo apt install git -y

echo ""
echo "====================================";
echo "Instalare homebrew en su sitema.";
echo "====================================";
echo ""

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

echo ""
echo "====================================";
echo "Agregare homebrew al path del sistema.";
echo "====================================";
echo ""

eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
echo 'export PATH="/home/linuxbrew/.linuxbrew/bin:$PATH"' >>~/.profile^C
echo 'export MANPATH="/home/linuxbrew/.linuxbrew/share/man:$MANPATH"' >>~/.profile
echo 'export INFOPATH="/home/linuxbrew/.linuxbrew/share/info:$INFOPATH"' >>~/.profile
source ~/.profile


echo ""
echo "====================================";
echo "Revisare que homebrew este instlado correctamente.";
echo "====================================";
echo ""

brew doctor

echo ""
echo "====================================";
echo "Antes de instalar mkcer, necesito instalar libnss3-tools en su computadora.";
echo "====================================";
echo ""

apt-get install wget libnss3-tools

echo ""
echo "====================================";
echo "Ahora si, procedo a instalr mkcert.";
echo "====================================";
echo ""

brew install mkcert


echo ""
echo "====================================";
echo "Eso ha sido todo, graciar por usaar el instalador.";
echo "====================================";
echo ""


exit