# Dreams WordPress Docker Stack

Inicialización de Wordpress, MariaDB, Redis Object Cache

# Requerimientos previos

- **Docker Desktop**

    Se instala automaticamente al ejecutar el script
    Mas informacion en la seccion instalacion automatica
    
- **SwitchHost**

    https://github.com/oldj/SwitchHosts/releases

    <br>

- **mkcert**

    Se instala automaticamente al ejecutar el script
    Mas informacion en la seccion [instalacion automatica](# "Instalar ```Docker```, ```Docker compose```, ```homebew``` y ```mkcert``` de manera automatica.")
    <br>


# Instalar ```Docker```, ```Docker compose```, ```homebew``` y ```mkcert``` de manera automatica.

- **PASO 1: Clonamos este branch en la carpeta deseada de nuestro ordenador con el siguiente comando.** 

        git clone --branch master git@gitlab.com:rodolfo29/dreams.wp.docker.stack.linux.git

- **PASO 2: Abrimos la carpeta scripts, donde encontraremos el archivo.**

    wordpress_docker.sh

    Nota:

    Antes de ejecutar el script es necesario agregarle los permisos de ejecucion con el comando `chmod +x wordpress_docker.sh`

- **PASO 3: Ejecutamos el script

    `./wordpress_docker.sh`

    Al finalizar el script, la computadora se reiniciara, no olvide guardar sus avances y cerrar las ventanas.


# Initial Setup

**Step 1:** Todas las opciones necesarias para configurar tu stack estarán en ```.env```, verifica estas opciones para poner las variables que se adecuen a tu proyecto. Lo unico que necesitas es copiar `.env-template` y cambiar el nombre a `.env`.

**Step 2:**  Requerimos crear un certificado ssl para usar https en nuestro stack ya sea local o en produccion.

**Step 3:**  Iniciar docker usando `docker compose`.


## Step 1: Environment Configuration

This stack can be configured for different environments using the ```.env``` dotfile. See ```.env-template``` for all variables available. Copy ```.env-template``` to ```.env``` and update as necessary. This is where you change database username and password, update nginx configuration, specify which docker image versions to use for each service, etc.

## Step 2: Development - Generate Local Certificates

Confirmar que este instalado: [mkcert](https://github.com/FiloSottile/mkcert#installation) y corre el comando necesario sobre la carpeta:  ```./certs``` . Recuerda actualizar tu `.env` antes de instalar el certificado.

```
$ mkcert -install
Created a new local CA 💥
The local CA is now installed in the system trust store! ⚡️
The local CA is now installed in the Firefox trust store (requires browser restart)! 🦊

$ mkcert wordpress.local "*.wordpress.local"

Created a new certificate valid for the following names 📜
 - "wordpress.local"
 - "*.wordpress.local"

The certificate is at "./wordpress.local+1.pem" and the key at "./wordpress.local+1-key.pem" ✅
```

### Step 3: Start Docker Stack

Recuerda abrir Docker Desktop y despues ejecutar el comando ```docker-compose up -d```

## Docker Compose Basics

Ver documentación en Docker [docker compose documentation.](https://docs.docker.com/compose/)

Este stack puede ser usado para desarrollo local, stage o producción.

Si es local vamos a necesitar mappear dominios con **Gas Mask**

![Gas Mask Mapping](https://i.ibb.co/4PwMVQT/Screen-Shot-2022-01-16-at-1-22-59-AM.png)

En este ejemplo podemos ver que estamos mappeando el dominio `wordpress.local`, para poder acceder con `https` recuerda verificar el Step 2.

Tambien agregamos algunas opciones para un ambiente desarrollo que podemos usar de la siguiente manera:

```
docker compose -f docker-compose.yml -f docker-compose.dev.yml up -d
```


**Common Docker Compose Commands**

```
docker-compose up -d            # start services

docker-compose ps               # view service info

docker-compose logs nginx       # view nginx logs 

docker-compose stop             # stop containers

docker-compose rm               # remove containers
```

## Deploy Static Docker Image

Tambien como opcion creamos un `Dockerfile`, para aplicarlo a nuestros procesos de CI/CD .
